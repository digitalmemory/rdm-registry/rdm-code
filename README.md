# OAIS Registry

## Local development

As a requirement, you need `invenio-cli`, install it with `pip install invenio-cli`.

Now, to run the InvenioRDM instance locally:

```bash
# needed because node18
export NODE_OPTIONS=--openssl-legacy-provider

invenio-cli install
invenio-cli services setup

invenio-cli run

# in another terminal:
invenio-cli assets build
# or watch
invenio-cli assets watch


# workaround for watch to pick up changes to the less files
ln -s assets/less/cds-rdm ~/.virtualenvs/oais-registry-KEvpN4yF/var/instance/less
```

Visit https://127.0.0.1:5000


## For deployment

To push the local version to the container registry:

```
docker build -t gitlab-registry.cern.ch/digitalmemory/openshift-deploy/oais-registry-rdm:qa .
docker push gitlab-registry.cern.ch/digitalmemory/openshift-deploy/oais-registry-rdm:qa
```

To push the tested and stable qa image to the container registry (for prod deployment):

```
docker tag gitlab-registry.cern.ch/digitalmemory/openshift-deploy/oais-registry-rdm:qa gitlab-registry.cern.ch/digitalmemory/openshift-deploy/oais-registry-rdm:stable
docker push gitlab-registry.cern.ch/digitalmemory/openshift-deploy/oais-registry-rdm:stable
```

See [Helm charts](https://gitlab.cern.ch/digitalmemory/rdm-registry/oais-registry-rdm) for deployment.

## Troubleshooting

If the services can't be brought up, run `invenio-cli services stop` and `docker-compose down` before running `invenio-cli run` again.
Check also `docker ps` that you don't have similar pods up from other instances.

To reset the instance, run `invenio-cli services destroy`.
